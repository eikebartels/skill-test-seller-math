FROM python:3.4

WORKDIR /usr/src/
COPY requirements.txt .
RUN pip install --upgrade pip setuptools wheel
RUN pip install -r requirements.txt
# must be installed 
RUN python -m pip install numpy==1.16.6
RUN pip install pandas

# Default powerline10k theme, no plugins installed
RUN sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.2/zsh-in-docker.sh)"
