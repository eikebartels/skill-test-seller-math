from django.contrib import admin

from .models import Upload

class ImportAdmin(admin.ModelAdmin):
    list_display = ["created_at", "file"]

admin.site.register(Upload, ImportAdmin)
