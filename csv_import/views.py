from django.shortcuts import HttpResponseRedirect, render

from csv_import.form import CsvUploadForm
from django.views.generic.list import ListView

from orders.models import Order

def upload_csv(request):
    if request.method == 'POST':
        form = CsvUploadForm(request.POST, request.FILES)
        print("form", form.is_valid(), form.errors, request.FILES)
        if form.is_valid():
            model = form.save()
            return HttpResponseRedirect("/success/{}/".format(model.id))
    else:
        form = CsvUploadForm()
    return render(request, 'upload_csv.html', {'form': form})


# NOTE: Using ListView for simplicity. We could also use a custom view and pass in for example infos about the import it self
class ImportListView(ListView):
    template_name = 'display_csv.html'
    model = Order

    def get_context_data(self, **kwargs):
        context = super(ImportListView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        queryset = Order.objects.select_related("shipping_address").filter(csv_import_id=self.kwargs["id"]).prefetch_related() #.values('shipping_address__zip')
        print(queryset.query)
        return queryset

    