import csv
from django.db import models
import pandas as pd
from datetime import datetime

from orders.models import Address, Customer, Item, Order

class Upload(models.Model):
    file = models.FileField(upload_to='uploads/')
    created_at = models.DateField(auto_now_add=True)

    def save(self, *args, **kwargs):
        creating = self.created_at
        super().save(*args, **kwargs)
        if creating is None:
            df=pd.read_csv(self.file,sep=';',encoding='latin-1',dtype={'fulfillment_transaction_code': str})
            
            df = df.fillna('')
            orders = []
            for _, row in df.iterrows():
                shipping_address_id, billing_address_id = get_or_create_addresses(row)

                order = Order(
                    csv_import = self,
                    order_number_shop=row["order_number_shop"],
                    order_number_idealo=row["order_number_idealo"],
                    created_at = datetime.strptime(row["created_at"], "%d.%m.%Y %H:%M").date(),
                    processed_at = datetime.strptime(row["processed_at"], "%d.%m.%Y %H:%M").date(),
                    status = row["status"],
                    currency = row["currency"],
                    quantity= row["quantity"],
                    shipping_address_id = shipping_address_id,
                    billing_address_id = billing_address_id,
                    item_id = get_or_create_item(row),

                    merchant_id= row["merchant_id"], 
                    merchant_name= row["merchant_name"], 
                    voucher_code= row["voucher_code"], 
                    refund_amounts= get_int_or_0(row["refund_amounts"]), 
                    refund_ids= row["refund_ids"], 

                    total_shipping=get_int_or_0(row["total_shipping"]),
                    fulfillment_carrier=row["fulfillment_carrier"],
                    fulfillment_type=row["fulfillment_type"],
                    fulfillment_transaction_code=row["fulfillment_transaction_code"],
                    fulfillment_options=row["fulfillment_options"],
                    estimated_delivery_time= row["delivery_time"], 

                    payment_method = row["payment_method"],
                    payment_transaction_id = row["payment_transaction_id"],

                    customer_id = get_or_create_customer(row),
                )
                orders.append(order)
            
            Order.objects.bulk_create(orders)
        


def get_or_create_addresses(row):
    shipping_address = {
        "first_name": row["shipping_address_given_name"],
        "last_name": row["shipping_address_family_name"],
        "address1": row["shipping_address_address1"],
        "address2": row["shipping_address_address2"],
        "city": row["shipping_address_city"],
        "country": row["shipping_address_country"],
        "zip": row["shipping_address_zip"],
        "salutation": row["shipping_address_salutation"],
    }
    billing_address = {
        "first_name": row["billing_address_given_name"],
        "last_name": row["billing_address_family_name"],
        "address1": row["billing_address_address1"],
        "address2": row["billing_address_address2"],
        "city": row["billing_address_city"],
        "country": row["billing_address_country"],
        "zip": row["billing_address_zip"],
        "salutation": row["billing_address_salutation"],
    }

    if billing_address == shipping_address:
        shipping_address_id = Address.objects.create(**shipping_address).id
        billing_address_id = shipping_address_id
    else:
        shipping_address_id = Address.objects.create(**shipping_address).id
        billing_address_id = Address.objects.create(**billing_address).id

    return (shipping_address_id, billing_address_id)

def get_or_create_item(row):
    try:
        item = Item.objects.get(sku = row['sku'])
    except Item.DoesNotExist:
        item = Item.objects.create(
            title=row["title"],
            sku=row["sku"],
            price=row["item_price"],
            former_price=get_int_or_0(row["former_price"]),
            price_range_amount=row["price_range_amount"],
        )
    
    return item.id

def get_int_or_0(string_value):
    try:
        value = int(string_value)
    except:
        value = 0
    return value

def get_or_create_customer(row):
    try:
        customer = Customer.objects.get(email = row['customer_email'])
    except Customer.DoesNotExist:
        customer = Customer.objects.create(
            email=row["customer_email"],
            phone=row["customer_phone"],
        )
    
    return customer.id