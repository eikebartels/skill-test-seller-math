from django.forms import ModelForm
from django import forms
from csv_import.models import Upload

class CsvUploadForm(ModelForm):
    class Meta:
        model = Upload
        fields = ['file'] 
