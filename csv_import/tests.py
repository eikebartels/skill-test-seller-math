


from csv_import.models import Upload
from django.core.files import File
import pytest

@pytest.mark.django_db
def test_file_upload():
    file = File(open("test_media/dataset.csv"))
    object = Upload.objects.create(file=file);
    assert object.created_at is not None
    print(vars(object))
    