from django.contrib import admin

from .models import Address, Customer, Order, Item

class AddressAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Address._meta.get_fields()]
admin.site.register(Address, AddressAdmin)

class CustomerAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Customer._meta.get_fields()]
admin.site.register(Customer, CustomerAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Order._meta.get_fields()]
admin.site.register(Order, OrderAdmin)

class ItemAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Item._meta.get_fields()]
admin.site.register(Item, ItemAdmin)
