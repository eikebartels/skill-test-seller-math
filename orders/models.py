from email.policy import default
import uuid
from django.db import models


class Order(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    order_number_shop = models.CharField(max_length=35)
    order_number_idealo = models.CharField(max_length=35)

    created_at = models.DateField(auto_now_add=True)
    processed_at = models.DateField(blank=True)

    total_shipping = models.PositiveIntegerField()
    fulfillment_carrier = models.CharField(max_length=35, blank=True)
    fulfillment_type = models.CharField(
        choices=(
            ("POSTAL", "POSTAL"),
        ),
        default=None,
        max_length=3,
        blank=True
    )
    fulfillment_transaction_code = models.CharField(max_length=35, blank=True)
    fulfillment_options = models.CharField(max_length=35, blank=True)


    customer = models.ForeignKey(
        'Customer',
        on_delete=models.CASCADE,
        related_name='+',
    )

    shipping_address = models.ForeignKey(
        'Address',
        on_delete=models.CASCADE,
        related_name='+',
    )

    billing_address = models.ForeignKey(
        'Address',
        on_delete=models.CASCADE,
        related_name='+',
    )

    status = models.CharField(
        choices=(
            ("PROCESSING", "PROCESSING"),
            ("COMPLETED", "COMPLETED"),
            ("REVOKED", "REVOKED"),
        ),
        default="PROCESSING",
        max_length=10
    )

    estimated_delivery_time = models.CharField(blank=True, max_length=30) # delivery_time

    currency = models.CharField(
        choices=(
            ("EUR", "EUR"),
        ),
        default="EUR",
        max_length=3
    )


    # Simplification of the order items. See Readme for more informations
    item = models.ForeignKey(
        'Item',
        on_delete=models.PROTECT,
    )

    quantity = models.PositiveIntegerField(default=0)

    merchant_id = models.CharField(max_length=35)
    merchant_name = models.CharField(max_length=35)

    voucher_code = models.CharField(max_length=35)

    refund_amounts = models.IntegerField(default=0)
    refund_ids = models.CharField(max_length=35)

    payment_method = models.CharField(
        choices=(
            ("PAYPAL", "PAYPAL"),
        ),
        default="PAYPAL",
        max_length=5
    )

    payment_transaction_id = models.CharField(max_length=35)

    csv_import = models.ForeignKey("csv_import.Upload", on_delete=models.CASCADE)

    @property
    def total_item_price(self):
        return self.quantity * self.item.price

    @property
    def total_price_without_shipping(self):
        return self.total_item_price

    @property
    def total_price_with_shipping(self):
        return self.total_item_price + self.total_shipping

class Address(models.Model):
    first_name = models.CharField(max_length=35)
    last_name = models.CharField(max_length=35)
    address1 = models.CharField(max_length=100)
    address2 = models.CharField(max_length=100)
    city = models.CharField(max_length=189)
    country = models.CharField(max_length=90)
    zip = models.CharField(max_length=11)
    salutation = models.CharField(max_length=30)


class Customer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.CharField(max_length=320)
    phone = models.CharField(max_length=50)

   

class Item(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=320)
    sku = models.CharField(max_length=64)
    price = models.PositiveIntegerField()
    former_price = models.PositiveIntegerField(blank=True, null=True)
    price_range_amount = models.CharField(max_length=64, blank=True)