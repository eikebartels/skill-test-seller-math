# Skill test seller math

## Dependencies 
- python3.4 
- Django==1.11.3

## Task 

1. Create a page to upload a csv 
2. Store the csv data set in a database 
3. Create an additional page to display the uploaded data in a table with the following columns: 
- shipping_address_zip (PLZ)
- total_price_with_shipping (Summe)
4. The table should be sortable by clicking the column header 


## Example Data Set (anonymized)
```csv
order_number_idealo;order_number_shop;created_at;processed_at;status;currency;total_price_with_shipping;total_price_without_shipping;total_shipping;customer_email;customer_phone;shipping_address_address1;shipping_address_address2;shipping_address_city;shipping_address_country;shipping_address_given_name;shipping_address_family_name;shipping_address_zip;shipping_address_salutation;billing_address_address1;billing_address_address2;billing_address_city;billing_address_country;billing_address_given_name;billing_address_family_name;billing_address_zip;billing_address_salutation;fulfillment_carrier;fulfillment_type;fulfillment_transaction_code;fulfillment_options;payment_method;payment_transaction_id;merchant_id;merchant_name;total_item_price;item_price;former_price;price_range_amount;quantity;sku;title;delivery_time;voucher_code;refund_ids;refund_amounts;refund_statuses
ELYD9KVN;ELYD9KVN Idealo;28.02.2022 22:51;28.02.2022 22:53;COMPLETED;EUR;8924;8924;0;m-xxxx@checkout.idealo.de;;Strasse 1;;München;DE;Hans;Hubert;61169;MRS;Strasse 1;;München;DE;Hans;Hubert;61169;MRS;dhl-germany;POSTAL;00344346505447929;;PAYPAL;6LM4444Y6366490A;;;8924;8924;;;1;MB-FER-FG444;Le Passe-Trappe: Groese (98 x 53 cm);1 - 3 Werktage;;;;
```

The full data set is because of privacy reasons not provided


## Challenges
1. The python version is from 2014 which causes issues with apple silicon. To overcome this issue, I'm using docker to spin up the development enviroment. 

2. 



## Simplifications 

- No state machine: In a real world application you would not status field in the order table. Instead you would define a fine grand state machine which tracks and controlls all status changes. 
- The order model usually not contain the items directly. Instead a join table should be used with forign reference to a item table and the quantity
- Merchant should be go in its own table.
- The refund column should be in a extra table

## Questions 

- Is Idealo not a mercent? order_number_idealo should create though order_nummer and mercent name
- refund_ids? How does this look like? Why do we have an array? 
- former_price & price_range_amount -> not sure where there belong to. Items?

## Not handled 
- upload encoding detection 



# How to run this application
- Docker has to be installed 
- Build the image 
```
    docker build -t skill-test-dev .
```
- Spin up the container. Note: Only tested on Mac Silicon. On other systems `--network host` might be needed
```
docker run -p 8000:8000 -it --name myapp --rm \
    --volume $(pwd):/usr/src/ \
    myapp-dev:latest \
    sh
```

- run migration
```
python manage.py migrate
```

- start django application 

```
python manage.py runserver 0.0.0.0:8000
```

- if needed, a superuser can be create
```
python manage.py createsuperuser 
```
